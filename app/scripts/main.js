$(document).ready(function(){

  $('.js-main-latest-game-select').select2();
  
  $('.owl-carousel').owlCarousel({
      loop: true,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      smartSpeed: 450,
      dots: false,
      nav: true,
      navText:[
          '<span class=\'glyphicon glyphicon-chevron-left\'></span>',
          '<span class=\'glyphicon glyphicon-chevron-right\'></span>'
      ],
      responsive: {
          0: {
              items: 1
          }
      }
  });
});